package com.wurmonline.ulviirala.mods;

import javassist.*;
import javassist.bytecode.Descriptor;
import javassist.expr.ExprEditor;
import javassist.expr.MethodCall;
import org.gotti.wurmunlimited.modloader.classhooks.HookManager;
import org.gotti.wurmunlimited.modloader.interfaces.Configurable;
import org.gotti.wurmunlimited.modloader.interfaces.PreInitable;
import org.gotti.wurmunlimited.modloader.interfaces.Versioned;
import org.gotti.wurmunlimited.modloader.interfaces.WurmServerMod;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * Displays an estimate of the remaining burn time of a fire before it drops
 * to a certain temperature.
 */
public class FireBurnTime implements WurmServerMod, PreInitable, Configurable, Versioned {
    private static final Logger _Logger = Logger.getLogger(FireBurnTime.class.getName());
    private static final String version = "ty1.1";

    /**
     * Target temperature.
     *
     # 0    -  999   The fire is not lit.
     # 1000 - 1999	A few red glowing coals can be found under a bed of ashes.
     # 2000 - 3499	A layer of ashes is starting to form on the glowing coals.
     # 3500 - 3999	A hot red glowing bed of coal remains of the fire now.
     # 4000 - 4999	A few flames still dance on the fire, but soon they too will die.
     # 5000 - 6999	The fire is starting to fade.
     # 7000 - 8999	The fire burns with wild flames and still has much unburnt material.
     # 9000+			The fire burns steadily and will still burn for a long time.
     */
    public int _TargetTemperature = 5000;

    @Override
    public void preInit() {
        _Logger.info("Initialising FireBurnTime");

        try {
            CtClass ctClass = HookManager.getInstance().getClassPool().get("com.wurmonline.server.behaviours.FireBehaviour");
            CtClass[] parameters = new CtClass[] {
                    HookManager.getInstance().getClassPool().get("com.wurmonline.server.behaviours.Action"),
                    HookManager.getInstance().getClassPool().get("com.wurmonline.server.creatures.Creature"),
                    HookManager.getInstance().getClassPool().get("com.wurmonline.server.items.Item"),
                    CtPrimitiveType.shortType,
                    CtPrimitiveType.floatType
            };
            CtField randomField = CtField.make("private static final java.util.Random random = new java.util.Random();", ctClass);
            ctClass.addField(randomField);
            CtMethod ctMethod = ctClass.getMethod("action", Descriptor.ofMethod(CtPrimitiveType.booleanType, parameters));
            ctMethod.instrument(new ExprEditor() {
                @Override
                public void edit(MethodCall methodCall) throws CannotCompileException {
                    if (methodCall.getMethodName().equals("sendNormalServerMessage")) {
                        ctMethod.insertAt(methodCall.getLineNumber(),
                                "{" +
                                        "float coolingSpeed = 1.0f;"+
                                        "com.wurmonline.server.zones.VolaTile forgeTile = com.wurmonline.server.zones.Zones.getTileOrNull(target.getTilePos(), target.isOnSurface());"+
                                        "if (forgeTile != null && forgeTile.getStructure() != null)"+
                                        "    coolingSpeed *= 0.75f;"+
                                        "else if (com.wurmonline.server.Server.getWeather().getRain() > 0.2f)"+
                                        "    coolingSpeed *= 2f;"+

                                        "if (target.getRarity() > 0)"+
                                        "    coolingSpeed *= Math.pow(0.8999999761581421, (double)target.getRarity());"+

                                        "float decreaseTemperature = coolingSpeed * Math.max(1f, 11f - Math.max(1f, 20f * Math.max(30f, target.getCurrentQualityLevel()) / 200f));"+
                                        "int secondsRemaining = Math.round(Math.max(0, target.getTemperature() - " + _TargetTemperature + ") / decreaseTemperature);"+

                                        "try{"+
                                        "    if (target.getTemperature() > 1000) {"+
                                        "        sendString.append(' ');"+
                                        "        double firemakingSkill = performer.getSkills().getSkill(1010).getKnowledge();"+ // SkillList.FIREMAKING == 1010
                                        "        if (firemakingSkill < 5) {"+
                                        "            sendString.append(\"You have no idea how long the fire is going to last.\");"+
                                        "        } else {"+
                                        "            sendString.append(\"You think\");"+
                                        "            if (target.getTemperature() < "+_TargetTemperature+") {"+
                                        "                int secondsUntilAsh = (int)((target.getTemperature() - 1000) / decreaseTemperature);"+
                                        "                int hoursUntilAsh = secondsUntilAsh/3600;"+
                                        "                int minutesUntilAsh = (secondsUntilAsh%3600) / 60;"+
                                        "                sendString.append(\" you should put something on the fire before it's only a layer of ashes\");"+
                                        "                if(firemakingSkill >= 20){"+
                                        "                    if(firemakingSkill < 30){"+
                                        "                        sendString.append(\" in about \");"+
                                        "                    }else{" +
                                        "                        sendString.append(\" in \");" +
                                        "                    }"+
                                        "                    if(hoursUntilAsh > 1){"+
                                        "                        sendString.append(secondsUntilAsh/3600).append(\" hours\");"+
                                        "                    } else if (hoursUntilAsh == 1) {"+
                                        "                        sendString.append(\"1 hour\");"+
                                        "                    }"+
                                        "                    if(minutesUntilAsh > 0){"+
                                        "                        if(hoursUntilAsh > 0){"+
                                        "                            sendString.append(\", \");"+
                                        "                        }"+
                                        "                        if(minutesUntilAsh == 1){"+
                                        "                            sendString.append(\"1 minute\");"+
                                        "                        }else{"+
                                        "                            sendString.append(minutesUntilAsh).append(\" minutes\");"+
                                        "                        }"+
                                        "                    }"+
                                        "                    if(firemakingSkill >= 30) {"+
                                        "                        if (secondsUntilAsh % 60 > 0) {"+
                                        "                            if (minutesUntilAsh > 0 || hoursUntilAsh > 0) {"+
                                        "                                sendString.append(\" and \");"+
                                        "                            }"+
                                        "                            if (secondsUntilAsh % 60 == 1) {"+
                                        "                                sendString.append(\"1 second\");"+
                                        "                            } else {"+
                                        "                                sendString.append(secondsUntilAsh % 60).append(\" seconds\");"+
                                        "                            }"+
                                        "                        }"+
                                        "                    }"+
                                        "                    if(firemakingSkill >= 50){"+
                                        "                        sendString.append(\" and \").append(random.nextInt(1000)).append(\" milliseconds\");"+
                                        "                        if(firemakingSkill >= 70){"+
                                        "                            sendString.append(\" and \").append(random.nextInt(1000)).append(\" microseconds\");"+
                                        "                            if(firemakingSkill >= 90){"+
                                        "                                sendString.append(\" and \").append(random.nextInt(1000)).append(\" nanoseconds\");"+
                                        "                                if(firemakingSkill >= 99.99999){"+
                                        "                                    sendString.append(\" and \").append(random.nextInt(1000)).append(\" picoseconds\");"+
                                        "                                }"+
                                        "                            }"+
                                        "                        }"+
                                        "                    }"+
                                        "                }"+
                                        "            }"+
                                        "            else {"+
                                        "               sendString.append(\""+
                                            (_TargetTemperature > 8999 ? " the fire will burn steadily for " :
                                            _TargetTemperature > 6999 ? " the fire will have much unburnt material for " :
                                            _TargetTemperature > 4999 ? " the fire will not start to fade for " :
                                            _TargetTemperature > 3999 ? " the fire will only have a few dancing flames in " :
                                            _TargetTemperature > 1999 ? " the fire will only have a bed of red glowing coals in " :
                                            _TargetTemperature > 999 ?  " the fire will only be a layer of ash in " :
                                                                        " the fire will be completely cold in "
                                            )+"\");"+
                                        "                int hoursLeft = secondsRemaining / 3600;"+
                                        "                int minutesLeft = (secondsRemaining % 3600) /60;"+
                                        "                if(firemakingSkill >= 20){"+
                                        "                    if(firemakingSkill < 30){"+
                                        "                        sendString.append(\"about \");"+
                                        "                    }"+
                                        "                    if(hoursLeft > 1){"+
                                        "                        sendString.append(secondsRemaining/3600).append(\" hours\");"+
                                        "                    } else if (hoursLeft == 1) {"+
                                        "                        sendString.append(\"1 hour\");"+
                                        "                    }"+
                                        "                    if(minutesLeft >= 1){"+
                                        "                        if(hoursLeft > 0) {"+
                                        "                            sendString.append(\", \");"+
                                        "                        }"+
                                        "                        if(minutesLeft == 1){"+
                                        "                            sendString.append(\"1 minute\");"+
                                        "                        }else{"+
                                        "                            sendString.append(minutesLeft).append(\" minutes\");"+
                                        "                        }"+
                                        "                    }"+
                                        "                    if(firemakingSkill >= 30) {"+
                                        "                        if (secondsRemaining % 60 > 0) {"+
                                        "                            if (minutesLeft > 0 || hoursLeft > 0) {"+
                                        "                                sendString.append(\" and \");"+
                                        "                            }"+
                                        "                            if (secondsRemaining % 60 == 1) {"+
                                        "                                sendString.append(\"1 second\");"+
                                        "                            } else {"+
                                        "                                sendString.append(secondsRemaining % 60).append(\" seconds\");"+
                                        "                            }"+
                                        "                        }"+
                                        "                        if(firemakingSkill >= 50){"+
                                        "                            sendString.append(\" and \").append(random.nextInt(1000)).append(\" milliseconds\");"+
                                        "                            if(firemakingSkill >= 70){"+
                                        "                                sendString.append(\" and \").append(random.nextInt(1000)).append(\" microseconds\");"+
                                        "                                if(firemakingSkill >= 90){"+
                                        "                                    sendString.append(\" and \").append(random.nextInt(1000)).append(\" nanoseconds\");"+
                                        "                                    if(firemakingSkill >= 99.99999){"+
                                        "                                        sendString.append(\" and \").append(random.nextInt(1000)).append(\" picoseconds\");"+
                                        "                                    }"+
                                        "                                }"+
                                        "                            }"+
                                        "                        }"+
                                        "                    }"+
                                        "                }"+
                                        "                else if (firemakingSkill >= 10){"+
                                        "                    if(hoursLeft > 1){"+
                                        "                        sendString.append(\"over an hour\");"+
                                        "                    }else{"+
                                        "                        if(minutesLeft > 45){"+
                                        "                            sendString.append(\"over 45\");"+
                                        "                        } else if (minutesLeft > 30) {"+
                                        "                            sendString.append(\"over 30\");"+
                                        "                        } else if (minutesLeft > 15) {"+
                                        "                            sendString.append(\"over 15\");"+
                                        "                        } else{"+
                                        "                            sendString.append(\"less than 15\");"+
                                        "                        }"+
                                        "                        sendString.append(\" minutes\");"+
                                        "                    }"+
                                        "                }"+
                                        "                else{"+
                                        "                    if(hoursLeft > 1){"+
                                        "                        sendString.append(\"over an hour\");"+
                                        "                    }else{"+
                                        "                        sendString.append(\"less than an hour\");"+
                                        "                    }"+
                                        "                }"+
                                        "            }"+
                                        "            sendString.append('.');"+
                                        "        }"+
                                        "    }"+
                                        "}catch(com.wurmonline.server.skills.NoSuchSkillException e){}"+
                                    "}"
                        );
                    }
                }
            });
        } catch (CannotCompileException | NotFoundException ex) {
            _Logger.log(Level.SEVERE, "FireBurnTime could not be applied.", ex);
        }
    }

    @Override
    public void configure(Properties properties) {
        _TargetTemperature = Math.max(200, Math.min(9000, Integer.parseInt(properties.getProperty("targetTemperature", String.valueOf(_TargetTemperature)))));
        _Logger.log(Level.INFO, String.format("Burn time estimation will be for %d temperature.", _TargetTemperature));
    }

    @Override
    public String getVersion(){
        return version;
    }
}
